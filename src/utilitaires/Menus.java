package utilitaires;

import java.util.Scanner;
import static utilitaires.Hasard.aleat;

public class Menus {
    
     private static Scanner  clavier=new Scanner(System.in);
    
      public static int   menu (){
        
          System.out.print ("1: Combattre"); 
          System.out.print("  2: Se défendre");
          System.out.print("  3: Fuir"); 
          System.out.print(" Que faites vous ? ");
               
          return  clavier.nextInt();
     }  
      
       public static  int menuAleat() {
     
        int a=aleat(1,100);
        int cas=3;
        if ( a<85) cas=1; else if (a<95) cas=2;
        return cas;
       }
       
     
}