package utilitaires;
import entites.*;
import java.util.List;
import static jeu.EtatDuJeu.*;

public class Hasard {
    
    public static int aleat ( int min, int max)   { return min+(int) (Math.random()*(max-min+1) ); }
    
    public static  boolean  docWhoCombat() {return  aleat(1, 100)>50 || ! ilResteAuMoinsUnCompagnonValide() ;}
    public static boolean   orbeBleue() { return aleat(1,100)>95;}
    public static  boolean  leCampDuDocteurWhoAgitEnPremier() { return aleat(0,1)==1; }
   
    public static  Ood             unOodVivantAuHasard()              { return (Ood) unAuHasardParmi(lesOodsVivants());} 
    public static Compagnon  unCompagnonVivantAuHasard()   { return ( Compagnon)  unAuHasardParmi(lesCompagnonsVivants() );}
    public static Compagnon  unCompagnonValideAuHasard()   { return ( Compagnon)  unAuHasardParmi(lesCompagnonsValides() );}
    public static Compagnon  unCompagnonEtourdiAuHasard( ) { return ( Compagnon) unAuHasardParmi(lesCompagnonsEtourdis() );}
    
    private static Object unAuHasardParmi (List uneListe){
    
         if ( uneListe.isEmpty() ) return null;
         int index =aleat(0,uneListe.size()-1);
         return uneListe.get(index);
    }
      
}