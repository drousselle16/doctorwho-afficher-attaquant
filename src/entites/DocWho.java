package entites;

import static utilitaires.Hasard.*;

public class DocWho extends Personnage {
    
     private boolean   fuiteReussie             =   false;
     private boolean   defenseReussie        =   false;
     private int           cptImmun                 =        0;

     public DocWho()  { nbVies =   12;  force_max=8; }
 
    @Override
     public void              subitAttaque( int forceAttaque, Personnage attaquant){
     
       int degats=0; 
       
       if (attaquant  instanceof  Alpha) {      
           if ( aleat(1,100)>70)  {
              
               attaquant.subitAttaque(forceAttaque*3, this);
               return;
           }
       }
        
       if  ( ! defenseReussie  && ! estImmunise() ){
                 degats= forceAttaque < nbVies?forceAttaque:nbVies;
                 nbVies -= degats ;
       }  
       else {
           
                 if(defenseReussie) defenseReussie = false; 
                 if( estImmunise())  cptImmun=0;
       }    
       
     }
  
     @Override
     public Personnage choisitAdversaire(){ return unOodVivantAuHasard(); }
     
     @Override
     public void              affiche()         {  System.out.println("Docteur Who "+nbVies+" points de vies" );}
     
     public void              seDefend()        { 
     
          defenseReussie= aleat(0, 100)>50; 
         
          if ( aleat(0, 100)>60 && cptImmun==0){ 
              cptImmun=1;  
          }
     }
     
     public void              tenteDeFuir( )    { 
         
         fuiteReussie      = aleat(0, 100)>95 ;
                      
     } 
  
     public boolean        aFuit()                { return fuiteReussie; }
     public boolean        naPasFuit()         { return !fuiteReussie; }
     public boolean        estImmunise()    { return cptImmun==2; }
   
     public int                getCptImmun()                     { return cptImmun; }
     public void              setCptImmun(int cptImmun) { this.cptImmun = cptImmun; }  

    public boolean isDefenseReussie() {
        return defenseReussie;
    }
 
}
