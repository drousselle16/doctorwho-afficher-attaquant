package entites;

import static  utilitaires.Hasard.aleat;

public class Alpha  extends Ood{

    private int          cptAttaqueSpe  = 0;
    private boolean  plus3                = false;
    
    public Alpha()         { nbVies= aleat(21 ,25); }
    
    public Alpha(int id) { this(); this.id=id; }
  
    @Override
    public void            affiche() {  System.out.println("Alpha "+ nbVies + " points de vies");}

    @Override
    public void attaque() {
  
        if(aleat(1,10)>4) activeAttaqueSpe();
        super.attaque(); 
    }
   private void             activeAttaqueSpe(){ cptAttaqueSpe  = 2; plus3 = aleat(1,2)==1;}
   
    @Override
    public int determineForceAttaque() {

        int force=super.determineForceAttaque();

        if( attaqueSpeActif() ){
            cptAttaqueSpe--; 
            if ( plus3 ){ force= force+5; } else { force=0;}
        }
        return force; 
    }    
    private boolean attaqueSpeActif(){ return  cptAttaqueSpe>0; }

    @Override
    protected void afficherAttaqueSubie(Personnage attaquant, int forceAttaque, int degats) {
        if(attaquant instanceof Compagnon){
            Compagnon comp = (Compagnon)attaquant;
            System.out.print(comp.getNom()+" attaque Alpha "+" f="+forceAttaque+" d="+degats);
        }
        else{
            System.out.print("DocWho attaque Alpha "+" f="+forceAttaque+" d="+degats);
        }
    }
    
    
}

