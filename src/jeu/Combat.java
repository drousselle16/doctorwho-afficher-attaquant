package jeu;

import entites.Compagnon;
import static jeu.EtatDuJeu.*;
import static  utilitaires.Menus.*;
import static  utilitaires.Affichages.*;
import static utilitaires.Hasard.*;

public class Combat {
 
   void principal(){
      
          while(  leCombatNestPasFini() ) {
            
               affichageEtatDuJeu();
               phaseDeCombat();
               actualiserEtatDuJeu();
               System.out.println();
           } 
           affichageEtatDuJeu();
           System.out.println();System.out.println();
           afficherResultatDuCombat();
           System.out.println(); 
   }
       
       
   void phaseDeCombat() {
       
        if ( leCampDuDocteurWhoAgitEnPremier() ) { 
            actionCampDocWho(); 
            if ( ilResteAuMoinsUnOddVivant() ) actiondUnOod(); }
        else {  
            actiondUnOod();   
            if ( leDocWho.estVivant() ) actionCampDocWho();
        }   
   }
      
    void actionCampDocWho() {
        
        if( docWhoCombat() ) actionDuDocteur(); else  if( ilResteAuMoinsUnCompagnonValide() )  actiondUnCompagnon();
    }
   
   void actionDuDocteur() {
     
      if(orbeBleue()  ){
             System.out.print(" OB-");
             if(aleat(1, 4)==4|| ! ilResteAuMoinsUnCompagnonEtourdi() ) {leDocWho.ajouterUneVie();  System.out.print("dw ");}
             else if( ilResteAuMoinsUnCompagnonEtourdi()) {
                 Compagnon c= unCompagnonEtourdiAuHasard();
                 c.setEtourdi(false);
                 System.out.print(nAb.get(c.getNom()));}
     }
       switch ( menuAleat()) {
        
             case  1:   leDocWho.attaque( )        ;break;
             case  2:   leDocWho.seDefend()      ;break;
             case  3:   leDocWho.tenteDeFuir()   ;break;           
       }
   }
  
   void actiondUnCompagnon() { 
       
       unCompagnonValideAuHasard().attaque();
       
   
   }
   
   void actiondUnOod() {
    
     unOodVivantAuHasard().attaque();
       
   }
   
   public static void main(String[] args) { new Combat().principal(); }     

   
}