package jeu;

import entites.*;
import java.util.LinkedList;
import java.util.List;
import static  utilitaires.Hasard.aleat;

public class EtatDuJeu {
    
    public static int noPhase=1;
    
    public static     DocWho             leDocWho                   = new DocWho();
    public static     Ood[ ]                oods                           = new Ood[5];
    public static     Compagnon[ ]    compagnons                = new Compagnon[4];

    static { 
        
        for ( int i=0; i<oods.length; i++)  oods[ i ] = new Ood(i); 
        
        if( aleat(1,5)>3) {
            int p=aleat(0, 4);
            oods[p]=new Alpha(p);
        }
       
        compagnons[0]= new Compagnon("Rory Williams");
        compagnons[1]= new Compagnon("Jack Hartnet");
        compagnons[2]= new Compagnon("Mickey Smith");
        compagnons[3]= new Compagnon("Danny Pink");
    }
    
    public static boolean                 leCombatNestPasFini() {  
    
        return leDocWho.estVivant() && ilResteAuMoinsUnOddVivant() && leDocWho.naPasFuit();
    }
    
    public static List<Compagnon>  lesCompagnonsValides(){
    
         List<Compagnon> lc= new LinkedList();
         for(  Compagnon cp  : compagnons  ) { if ( cp.estValide() ) lc.add(cp);  }
         return lc;
    }
    
    public static boolean                  ilResteAuMoinsUnCompagnonValide() { return lesCompagnonsValides().size()>0; }
    
    public static List<Compagnon>   lesCompagnonsVivants(){
          
        List<Compagnon> lc= new LinkedList();
        for( Compagnon cp : compagnons){ if( cp.estVivant()) lc.add(cp);}
        return lc;
    }
    
    public static boolean                  ilResteAuMoinsUnCompagnonEtourdi() { return lesCompagnonsEtourdis().size()>0; }
    
    public static List<Compagnon>   lesCompagnonsEtourdis(){
          
        List<Compagnon> lc= new LinkedList();
        for( Compagnon cp : compagnons){ if( cp.isEtourdi()&&cp.estVivant() ) lc.add(cp);}
        return lc;
    }
    
    public static boolean                  ilResteAuMoinsUnCompagnonVivant() { return  lesCompagnonsVivants().size()>0; }  
     
     public static List<Ood>             lesOodsVivants(){
          
        List<Ood> lo= new LinkedList();
        for( Ood oo : oods) { if( oo.estVivant()) lo.add(oo);}
        return lo;
    }
 
    public static boolean                  ilResteAuMoinsUnOddVivant()  { return  !lesOodsVivants().isEmpty();}
    
    public static void                       actualiserEtatDuJeu()    { 
        noPhase++;
        if          ( leDocWho.getCptImmun()==1 )         leDocWho.setCptImmun(2); 
        else if  ( leDocWho.getCptImmun()==2 )         leDocWho.setCptImmun(0);
    }
} 